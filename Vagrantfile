Vagrant.require_version ">= 1.5"
def which(cmd)
    exts = ENV['PATHEXT'] ? ENV['PATHEXT'].split(';') : ['']
    ENV['PATH'].split(File::PATH_SEPARATOR).each do |path|
        exts.each { |ext|
            exe = File.join(path, "#{cmd}#{ext}")
            return exe if File.executable? exe
        }
    end
    return nil
end

Vagrant.configure("2") do |config|

    config.vm.provider :virtualbox do |v|
        v.name = "knipster"
        v.customize [
            "modifyvm", :id,
            "--name", "knipster",
            "--memory", 4096,
            "--natdnshostresolver1", "on",
            "--cpus", 2,
        ]
    end

    config.vm.box = "debian/jessie64"

    config.vm.network :private_network, ip: "10.0.0.60"
    config.vm.hostname = "knipster.dev"

    config.ssh.forward_agent = true

    if which('ansible-playbook')
        config.vm.provision "ansible" do |ansible|
            ansible.playbook = "provision/playbook.yml"
            ansible.inventory_path = "provision/inventories/servers"
            ansible.limit = 'all'
        end
    end

    config.vm.synced_folder "./", "/var/www", type: "nfs"
end
