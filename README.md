Knipster
========

## Requirements
- Vagrant
- Ansible (2.1)
- Virtual Box (5.*)
- vagrant-hostsupdater plugin (optional)


## Environment setup
To install ansible run:
```
sudo apt-get install python-setuptools python-pip
sudo pip install ansible==2.1

```

To install vagrant hosts updater plugin run:
```
vagrant plugin install vagrant-hostsupdater

```

Finally run `vagrant up` and everything should be installed.

if everything went OK, you should be able to ping `knipster.dev`, but now it will show an error.
Follow the app installation proccess below to make it work.

## Instalation

When box is up, ssh into it and run following commands (from /var/www):

```
composer install
bin/console doctrine:schema:update --force
bin/console doctrine:fixtures:load

```

Right now you should be able to see documentation on `knipster.dev/api/doc`, and make requests.

## Running tests
```
bin/console doctrine:schema:update --force --env=test
vendor/bin/phpunit

```
