<?php


namespace Knipster\AppBundle\DataFixtures\ORM;


use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Knipster\AppBundle\Entity\Location\Country;

/**
 * Class LoadCountryData
 *
 * @package Knipster\AppBundle\DataFixtures\ORM
 */
class LoadCountryData extends AbstractFixture
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $country = new Country();
        $country->setCode('MT');
        $country->setName('Malta');
        $manager->persist($country);

        $country = new Country();
        $country->setCode('RS');
        $country->setName('Serbia');
        $manager->persist($country);

        $manager->flush();
    }
}