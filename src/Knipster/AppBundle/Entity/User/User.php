<?php


namespace Knipster\AppBundle\Entity\User;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Knipster\AppBundle\Entity\Location\Country;
use Knipster\AppBundle\Entity\Money\Transaction;
use Knipster\AppBundle\Entity\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


/**
 * Class User
 *
 * @ORM\Entity()
 *
 * @ORM\HasLifecycleCallbacks()
 *
 * @UniqueEntity("email", message="The email is already used")
 *
 * @JMS\ExclusionPolicy("all")
 *
 * @package Knipster\AppBundle\Entity
 */
class User
{
    use TimestampableEntity;

    const GENDER_MALE = 'm';
    const GENDER_FEMALE = 'f';

    const BONUS_PERCENT_MIN = 5;
    const BONUS_PERCENT_MAX = 20;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     *
     * @JMS\Expose()
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     *
     * @JMS\Expose()
     */
    protected $gender;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     *
     * @JMS\Expose()
     */
    protected $firstName;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $lastName;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     *
     * @JMS\Expose()
     */
    protected $email;

    /**
     * @var Country
     *
     * @ORM\ManyToOne(targetEntity="Knipster\AppBundle\Entity\Location\Country", inversedBy="users")
     *
     * @JMS\Expose()
     */
    protected $country;

    /**
     * @var Transaction[]
     *
     * @ORM\OneToMany(targetEntity="Knipster\AppBundle\Entity\Money\Transaction", mappedBy="user")
     */
    protected $transactions;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     *
     * @JMS\Expose()
     */
    protected $bonusPercent;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    protected $transactionsLocked = false;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->transactions = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set gender
     *
     * @param string $gender
     *
     * @return User
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set country
     *
     * @param Country $country
     *
     * @return User
     */
    public function setCountry(Country $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Add transaction
     *
     * @param Transaction $transaction
     *
     * @return User
     */
    public function addTransaction(Transaction $transaction)
    {
        $this->transactions[] = $transaction;

        return $this;
    }

    /**
     * Remove transaction
     *
     * @param Transaction $transaction
     */
    public function removeTransaction(Transaction $transaction)
    {
        $this->transactions->removeElement($transaction);
    }

    /**
     * Get transactions
     *
     * @return Transaction[]
     */
    public function getTransactions()
    {
        return $this->transactions;
    }

    /**
     * @return int
     */
    public function getBonusPercent()
    {
        return $this->bonusPercent;
    }

    /**
     * @param int $bonusPercent
     *
     * @return User
     */
    public function setBonusPercent($bonusPercent)
    {
        $this->bonusPercent = $bonusPercent;

        return $this;
    }

    /**
     * @return bool
     */
    public function isTransactionsLocked()
    {
        return $this->transactionsLocked;
    }

    /**
     * Lock user transactions
     *
     * @return User
     */
    public function lockTransactions()
    {
        $this->transactionsLocked = true;

        return $this;
    }

    /**
     * Unlock user transactions
     *
     * @return User
     */
    public function unlockTransactions()
    {
        $this->transactionsLocked = false;

        return $this;
    }
}
