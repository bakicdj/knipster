<?php


namespace Knipster\AppBundle\Entity\Money;


use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Knipster\AppBundle\Entity\Traits\TimestampableEntity;
use Knipster\AppBundle\Entity\User\User;

/**
 * Class Transaction
 *
 * @ORM\Entity(repositoryClass="Knipster\AppBundle\Repository\Money\TransactionRepository")
 * @ORM\HasLifecycleCallbacks()
 *
 * @JMS\ExclusionPolicy("all")
 *
 * @package Knipster\AppBundle\Entity\Money
 */
class Transaction
{
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     *
     * @JMS\Expose()
     */
    protected $id;

    /**
     * @var float
     *
     * @ORM\Column(type="decimal", precision=10, scale=2)
     *
     * @JMS\Expose()
     */
    protected $amount;

    /**
     * @var float
     *
     * @ORM\Column(type="decimal", precision=10, scale=2)
     *
     * @JMS\Expose()
     */
    protected $bonusAmount = 0;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="Knipster\AppBundle\Entity\User\User", inversedBy="transactions")
     */
    protected $user;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set amount
     *
     * @param float $amount
     *
     * @return Transaction
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @return float
     */
    public function getBonusAmount()
    {
        return $this->bonusAmount;
    }

    /**
     * @param float $bonusAmount
     *
     * @return Transaction
     */
    public function setBonusAmount($bonusAmount)
    {
        $this->bonusAmount = $bonusAmount;

        return $this;
    }

    /**
     * Set user
     *
     * @param User $user
     *
     * @return Transaction
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Is user withdrawing money
     *
     * @return bool
     */
    public function isWithdrawal()
    {
        return $this->getAmount() < 0;
    }

    /**
     * Is user making deposit
     *
     * @return bool
     */
    public function isDeposit()
    {
        return $this->getAmount() > 0;
    }
}
