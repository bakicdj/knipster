<?php


namespace Knipster\AppBundle\DTO\Report;


use Knipster\AppBundle\Exception\InvalidArgumentException;
use Symfony\Component\HttpFoundation\Request;


/**
 * Class ReportFilter
 *
 * @package Knipster\AppBundle\DTO\Report
 */
final class ReportFilter
{
    /**
     * @var \DateTime
     */
    private $dateFrom;

    /**
     * @var \DateTime
     */
    private $dateTo;

    /**
     * ReportFilter constructor.
     *
     * @param \DateTime $dateFrom
     * @param \DateTime $dateTo
     *
     * @throws InvalidArgumentException
     */
    private function __construct(\DateTime $dateFrom, \DateTime $dateTo)
    {
        if ($dateTo < $dateFrom) {
            throw new InvalidArgumentException('Invalid time frame');
        }

        $this->dateFrom = $dateFrom;
        $this->dateTo   = $dateTo;
    }

    /**
     * Create new ReportFilter instance from Symfony Request
     *
     * @param Request $request
     *
     * @return static
     */
    public static function createFromSymfonyRequest(Request $request)
    {
        $dateFrom = new \DateTime($request->query->get('from', '7 days ago'));
        $dateTo   = new \DateTime($request->query->get('to', 'now'));

        return new static($dateFrom, $dateTo);
    }

    /**
     * @return \DateTime
     */
    public function getDateFrom()
    {
        return $this->dateFrom;
    }

    /**
     * @return \DateTime
     */
    public function getDateTo()
    {
        return $this->dateTo;
    }
}