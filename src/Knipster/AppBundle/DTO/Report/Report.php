<?php


namespace Knipster\AppBundle\DTO\Report;


/**
 * Class Report
 *
 * @package Knipster\AppBundle\DTO\Report
 */
final class Report
{
    /**
     * @var string
     */
    private $date;

    /**
     * @var string
     */
    private $countryCode;

    /**
     * @var int
     */
    private $uniqueCustomers;

    /**
     * @var int
     */
    private $depositsCount;

    /**
     * @var float
     */
    private $totalDepositAmount;

    /**
     * @var int
     */
    private $withdrawalsCount;

    /**
     * @var float
     */
    private $totalWithdrawalAmount;

    /**
     * Report constructor.
     *
     * @param string $date
     * @param string $countryCode
     * @param int    $uniqueCustomers
     * @param int    $depositsCount
     * @param float  $totalDepositAmount
     * @param int    $withdrawalsCount
     * @param float  $totalWithdrawalAmount
     */
    private function __construct(
        $date,
        $countryCode,
        $uniqueCustomers,
        $depositsCount,
        $totalDepositAmount,
        $withdrawalsCount,
        $totalWithdrawalAmount
    )
    {
        $this->date                  = $date;
        $this->countryCode           = $countryCode;
        $this->uniqueCustomers       = $uniqueCustomers;
        $this->depositsCount         = $depositsCount;
        $this->totalDepositAmount    = $totalDepositAmount;
        $this->withdrawalsCount      = $withdrawalsCount;
        $this->totalWithdrawalAmount = $totalWithdrawalAmount;
    }

    /**
     * Create new Report instance from Mysql result
     *
     * @param array $result
     *
     * @return Report
     */
    public static function createFromMysqlResult(array $result)
    {
        return new static(
            $result['transaction_date'],
            $result['country_code'],
            $result['unique_customers'],
            $result['deposits_count'],
            $result['total_deposit_amount'],
            $result['withdrawals_count'],
            $result['total_withdrawals_amount']
        );
    }

    /**
     * @return string
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @return string
     */
    public function getCountryCode()
    {
        return $this->countryCode;
    }

    /**
     * @return int
     */
    public function getUniqueCustomers()
    {
        return $this->uniqueCustomers;
    }

    /**
     * @return int
     */
    public function getDepositsCount()
    {
        return $this->depositsCount;
    }

    /**
     * @return float
     */
    public function getTotalDepositAmount()
    {
        return $this->totalDepositAmount;
    }

    /**
     * @return int
     */
    public function getWithdrawalsCount()
    {
        return $this->withdrawalsCount;
    }

    /**
     * @return float
     */
    public function getTotalWithdrawalAmount()
    {
        return $this->totalWithdrawalAmount;
    }
}