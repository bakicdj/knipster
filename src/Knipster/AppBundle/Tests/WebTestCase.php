<?php


namespace Knipster\AppBundle\Tests;


use AppKernel;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\ORM\EntityManager;
use Knipster\AppBundle\Entity\Location\Country;
use Knipster\AppBundle\Entity\User\User;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase as BaseWebTestCase;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\Response;

require_once dirname(__DIR__).'../../../../app/AppKernel.php';

/**
 * Class WebTestCase
 *
 * @package Knipster\AppBundle\Tests
 */
class WebTestCase extends BaseWebTestCase
{
    /**
     * @var AppKernel
     */
    protected $kernelTest;

    /**
     * @var Container
     */
    protected $container;

    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var Client
     */
    protected $client;

    /**
     * Clear db
     */
    protected function setUp()
    {
        $this->kernelTest = new \AppKernel('test', true);
        $this->kernelTest->boot();
        $this->container = $this->kernelTest->getContainer();
        $this->em = $this->container->get('doctrine')->getManager();
        $this->client = static::createClient();

        parent::setUp();

        $purger = new ORMPurger($this->kernelTest->getContainer()->get('doctrine')->getManager());
        $this->container->get('doctrine')->getManager()->getConnection()->query(sprintf('SET FOREIGN_KEY_CHECKS=0'));
        $purger->setPurgeMode(ORMPurger::PURGE_MODE_DELETE);
        $purger->purge();
        $this->container->get('doctrine')->getManager()->getConnection()->query(sprintf('SET FOREIGN_KEY_CHECKS=1'));

        $application = new \Symfony\Bundle\FrameworkBundle\Console\Application($this->kernelTest);
        $application->setAutoExit(false);
    }

    /**
     * Generate full url based on path only
     *
     * @param $path
     *
     * @return string
     */
    protected function genUrl($path)
    {
        return 'http://knipster.dev/api_testing' . $path;
    }

    /**
     * Create web client
     *
     * @return Client
     */
    protected function getClient()
    {
        return self::createClient();
    }

    /**
     * Send get request
     *
     * @param string $path
     * @param array  $params
     * @param array  $headers
     *
     * @return null|Response
     */
    protected function get($path, $params = [], $headers = [])
    {
        $path .= '?'.http_build_query($params);
        
        return $this->sendRequest('GET', $path, $params, $headers);
    }

    /**
     * @param string $path
     * @param array  $params
     * @param array  $headers
     * @param array  $files
     *
     * @return null|Response
     */
    protected function post($path, array $params = [], array $headers = [], array $files = [])
    {
        return $this->sendRequest('POST', $path, $params, $headers, $files);
    }

    /**
     * @param string $path
     * @param array  $params
     * @param array  $headers
     *
     * @return null|Response
     */
    protected function put($path, array $params = [], array $headers = [])
    {
        return $this->sendRequest('PUT', $path, $params, $headers);
    }

    /**
     * @param string $path
     * @param array  $params
     * @param array  $headers
     *
     * @return null|Response
     */
    protected function patch($path, array $params = [], array $headers = [])
    {
        return $this->sendRequest('PATCH', $path, $params, $headers);
    }

    /**
     * @param string $path
     * @param array  $params
     * @param array  $headers
     *
     * @return null|Response
     */
    protected function delete($path, array $params = [], array $headers = [])
    {
        return $this->sendRequest('DELETE', $path, $params, $headers);
    }

    /**
     * Send request
     *
     * @param string $method
     * @param string $path
     * @param array  $params
     * @param array  $headers
     * @param array  $files
     *
     * @return null|Response
     */
    protected function sendRequest($method, $path,  array $params, array $headers, array $files = [])
    {
        $client = $this->getClient();

        $client->request($method, $this->genUrl($path), $params, $files, $headers);

        return $client->getResponse();
    }

    /**
     * Assert that response has status code 200
     *
     * @param Response $response
     */
    protected function assertOk(Response $response)
    {
        $this->assertTrue(
            $response->getStatusCode() === 200,
            'Response code is not 200 OK but: ' . $response->getStatusCode()
        );
    }

    /**
     * Assert that response has status code 400
     *
     * @param Response $response
     */
    protected function assertBadRequest(Response $response)
    {
        $this->assertTrue(
            $response->getStatusCode() === 400,
            'Response code is not 400 BAD REQUEST but: ' . $response->getStatusCode()
        );
    }

    /**
     * Assert that response has status code 201
     *
     * @param Response $response
     */
    protected function assertCreated(Response $response)
    {
        $this->assertTrue(
            $response->getStatusCode() === 201,
            'Response code is not 201 CREATED but: ' . $response->getStatusCode()
        );
    }

    /**
     * Create dummy user for testing
     *
     * @param Country $country
     * @param int     $bonusPercent
     *
     * @return User
     */
    protected function createDummyUser(Country $country, $bonusPercent = 10)
    {
        $user = new User();

        $user->setFirstName('First Name' . microtime(true));
        $user->setLastName('Last Name' . microtime(true));
        $user->setEmail(microtime(true).'@'.sha1(microtime(true)).'.com');
        $user->setCountry($country);
        $user->setBonusPercent($bonusPercent);
        $user->setGender(User::GENDER_MALE);

        $this->em->persist($user);
        $this->em->flush();

        $this->assertNotNull(
            $this->em->find(User::class, $user->getId()),
            'Dummy user not found in db'
        );

        return $user;
    }

    /**
     * Convert response body to array
     *
     * @param Response $response
     *
     * @return array
     */
    protected function toArray(Response $response)
    {
        return json_decode($response->getContent(), true);
    }
}