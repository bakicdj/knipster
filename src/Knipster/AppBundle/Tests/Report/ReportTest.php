<?php


namespace Knipster\AppBundle\Tests\Report;

use Knipster\AppBundle\Entity\Location\Country;
use Knipster\AppBundle\Entity\Money\Transaction;
use Knipster\AppBundle\Tests\WebTestCase;


/**
 * Class ReportTest
 *
 * @package Knipster\AppBundle\Tests\Report
 */
class ReportTest extends WebTestCase
{
    public function testReportsDefaultResult()
    {
        $malta = new Country();
        $malta->setCode('MT');
        $malta->setName('Malta');
        $this->em->persist($malta);

        $serbia = new Country();
        $serbia->setCode('RS');
        $serbia->setName('Serbia');
        $this->em->persist($serbia);

        $this->em->flush();

        $userFromMalta = $this->createDummyUser($malta);
        $userFromSerbia = $this->createDummyUser($serbia);
        $userFromSerbia2 = $this->createDummyUser($serbia);

        $transaction = new Transaction();
        $transaction->setUser($userFromMalta);
        $transaction->setAmount(100.00);
        $transaction->setCreatedAt(new \DateTime('10 days ago'));
        $this->em->persist($transaction);
        $this->em->flush($transaction);

        $transaction = new Transaction();
        $transaction->setUser($userFromMalta);
        $transaction->setAmount(100.00);
        $transaction->setCreatedAt(new \DateTime('5 days ago'));
        $this->em->persist($transaction);
        $this->em->flush($transaction);

        $transaction = new Transaction();
        $transaction->setUser($userFromSerbia);
        $transaction->setAmount(200.00);
        $transaction->setCreatedAt(new \DateTime('5 days ago'));
        $this->em->persist($transaction);
        $this->em->flush($transaction);

        $transaction = new Transaction();
        $transaction->setUser($userFromSerbia);
        $transaction->setAmount(100.00);
        $transaction->setCreatedAt(new \DateTime('1 days ago'));
        $this->em->persist($transaction);
        $this->em->flush($transaction);

        $transaction = new Transaction();
        $transaction->setUser($userFromSerbia2);
        $transaction->setAmount(100.00);
        $transaction->setCreatedAt(new \DateTime('1 days ago'));
        $this->em->persist($transaction);
        $this->em->flush($transaction);

        $transaction = new Transaction();
        $transaction->setUser($userFromSerbia2);
        $transaction->setAmount(-50.00);
        $transaction->setCreatedAt(new \DateTime('1 days ago'));
        $this->em->persist($transaction);
        $this->em->flush($transaction);

        $response = $this->get('/report');

        $responseArray = $this->toArray($response);

        $this->assertCount(3, $responseArray['data']);

        $this->assertEquals((new \DateTime('5 days ago'))->format('Y-m-d'), $responseArray['data'][0]['date']);
        $this->assertEquals('MT', $responseArray['data'][0]['countryCode']);
        $this->assertEquals(1, $responseArray['data'][0]['uniqueCustomers']);
        $this->assertEquals(1, $responseArray['data'][0]['depositsCount']);
        $this->assertEquals(100.00, $responseArray['data'][0]['totalDepositAmount']);
        $this->assertEquals(0, $responseArray['data'][0]['withdrawalsCount']);
        $this->assertEquals(0.00, $responseArray['data'][0]['totalWithdrawalAmount']);

        $this->assertEquals((new \DateTime('5 days ago'))->format('Y-m-d'), $responseArray['data'][1]['date']);
        $this->assertEquals('RS', $responseArray['data'][1]['countryCode']);
        $this->assertEquals(1, $responseArray['data'][1]['uniqueCustomers']);
        $this->assertEquals(1, $responseArray['data'][1]['depositsCount']);
        $this->assertEquals(200.00, $responseArray['data'][1]['totalDepositAmount']);
        $this->assertEquals(0, $responseArray['data'][1]['withdrawalsCount']);
        $this->assertEquals(0.00, $responseArray['data'][1]['totalWithdrawalAmount']);

        $this->assertEquals((new \DateTime('1 days ago'))->format('Y-m-d'), $responseArray['data'][2]['date']);
        $this->assertEquals('RS', $responseArray['data'][2]['countryCode']);
        $this->assertEquals(2, $responseArray['data'][2]['uniqueCustomers']);
        $this->assertEquals(2, $responseArray['data'][2]['depositsCount']);
        $this->assertEquals(200.00, $responseArray['data'][2]['totalDepositAmount']);
        $this->assertEquals(1, $responseArray['data'][2]['withdrawalsCount']);
        $this->assertEquals(-50.00, $responseArray['data'][2]['totalWithdrawalAmount']);
    }

    public function testFilter()
    {
        $malta = new Country();
        $malta->setCode('MT');
        $malta->setName('Malta');
        $this->em->persist($malta);

        $serbia = new Country();
        $serbia->setCode('RS');
        $serbia->setName('Serbia');
        $this->em->persist($serbia);

        $this->em->flush();

        $userFromMalta = $this->createDummyUser($malta);
        $userFromSerbia = $this->createDummyUser($serbia);
        $userFromSerbia2 = $this->createDummyUser($serbia);

        $transaction = new Transaction();
        $transaction->setUser($userFromMalta);
        $transaction->setAmount(100.00);
        $transaction->setCreatedAt(new \DateTime('10 days ago'));
        $this->em->persist($transaction);
        $this->em->flush($transaction);

        $transaction = new Transaction();
        $transaction->setUser($userFromMalta);
        $transaction->setAmount(100.00);
        $transaction->setCreatedAt(new \DateTime('5 days ago'));
        $this->em->persist($transaction);
        $this->em->flush($transaction);

        $transaction = new Transaction();
        $transaction->setUser($userFromSerbia);
        $transaction->setAmount(200.00);
        $transaction->setCreatedAt(new \DateTime('5 days ago'));
        $this->em->persist($transaction);
        $this->em->flush($transaction);

        $transaction = new Transaction();
        $transaction->setUser($userFromSerbia);
        $transaction->setAmount(100.00);
        $transaction->setCreatedAt(new \DateTime('1 days ago'));
        $this->em->persist($transaction);
        $this->em->flush($transaction);

        $transaction = new Transaction();
        $transaction->setUser($userFromSerbia2);
        $transaction->setAmount(100.00);
        $transaction->setCreatedAt(new \DateTime('1 days ago'));
        $this->em->persist($transaction);
        $this->em->flush($transaction);

        $transaction = new Transaction();
        $transaction->setUser($userFromSerbia2);
        $transaction->setAmount(-50.00);
        $transaction->setCreatedAt(new \DateTime('1 days ago'));
        $this->em->persist($transaction);
        $this->em->flush($transaction);

        $response = $this->get('/report', [
            'from' => (new \DateTime('10 days ago'))->format('Y-m-d'),
            'to' => (new \DateTime('8 days ago'))->format('Y-m-d')
        ]);

        $responseArray = $this->toArray($response);

        $this->assertCount(1, $responseArray['data']);

        $this->assertEquals((new \DateTime('10 days ago'))->format('Y-m-d'), $responseArray['data'][0]['date']);
        $this->assertEquals('MT', $responseArray['data'][0]['countryCode']);
        $this->assertEquals(1, $responseArray['data'][0]['uniqueCustomers']);
        $this->assertEquals(1, $responseArray['data'][0]['depositsCount']);
        $this->assertEquals(100.00, $responseArray['data'][0]['totalDepositAmount']);
        $this->assertEquals(0, $responseArray['data'][0]['withdrawalsCount']);
        $this->assertEquals(0.00, $responseArray['data'][0]['totalWithdrawalAmount']);
    }
}