<?php


namespace Knipster\AppBundle\Repository\Money;


use Doctrine\ORM\EntityRepository;
use Knipster\AppBundle\Entity\Money\Transaction;
use Knipster\AppBundle\Entity\User\User;

/**
 * Class TransactionRepository
 *
 * @package Knipster\AppBundle\Repository\Money
 */
class TransactionRepository extends EntityRepository
{
    /**
     * Get total count of user transactions
     *
     * @param User $user
     * @param bool $depositsOnly
     *
     * @return int
     */
    public function getCountForUser(User $user, $depositsOnly = true)
    {
        $qb = $this->getEntityManager()->createQueryBuilder()
            ->select('COUNT(t.id)')
            ->from(Transaction::class, 't')
            ->where('t.user = :user')
            ->setParameter('user', $user);

            if ($depositsOnly) {
                $qb->andWhere('t.amount > 0');
            }


        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * Get balance for given user
     *
     * @param User $user
     *
     * @return float
     */
    public function getBalance(User $user)
    {
        return $this->getEntityManager()->createQueryBuilder()
            ->select('SUM(t.amount)')
            ->from(Transaction::class, 't')
            ->where('t.user = :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * Generate report
     *
     * @param \DateTime $fromDate
     * @param \DateTime $toDate
     *
     * @return array
     */
    public function generateReport(\DateTime $fromDate, \DateTime $toDate)
    {
        $sql = "
            SELECT
              COUNT(DISTINCT  u.id) AS unique_customers,
              c.code AS country_code,
              SUM(IF(t.amount > 0, t.amount, 0)) AS total_deposit_amount,
              SUM(IF(t.amount < 0, t.amount, 0)) AS total_withdrawals_amount,
              COUNT(IF(t.amount > 0, t.id, NULL)) AS deposits_count,
              COUNT(IF(t.amount < 0, t.id, NULL)) AS withdrawals_count,
              DATE(t.created_at) AS transaction_date
            FROM transaction t
              INNER JOIN user u ON t.user_id = u.id
              INNER JOIN country c ON u.country_id = c.id
            WHERE DATE(t.created_at) >= :from_date AND DATE(t.created_at) <= :to_date
            GROUP BY transaction_date, country_code;
        ";

        $stmt = $this->getEntityManager()
            ->getConnection()
            ->prepare($sql);

        $formatedFromDate = $fromDate->format('Y-m-d');
        $formatedToDate   = $toDate->format('Y-m-d');

        $stmt->bindParam('from_date', $formatedFromDate);
        $stmt->bindParam('to_date', $formatedToDate);

        $stmt->execute();

        return $stmt->fetchAll();
    }
}