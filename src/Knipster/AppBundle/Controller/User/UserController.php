<?php


namespace Knipster\AppBundle\Controller\User;


use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\View\View;
use Knipster\AppBundle\Controller\BaseController;
use Knipster\AppBundle\Entity\User\User;
use Knipster\AppBundle\Form\Type\User\UserType;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class UserController
 *
 * @package Knipster\AppBundle\Controller
 */
class UserController extends BaseController
{
    /**
     * Register new user
     *
     * @Post("/users", name="kn.user.register")
     *
     * @ApiDoc(
     *    section="Users",
     *    description="Register new user",
     *    parameters={
     *       {"name" = "country", "dataType" = "object", "format" = "Country object {id : int, code: string, name: string}"}
     *    },
     *    input="Knipster\AppBundle\Form\Type\User\UserType",
     *    output="Knipster\AppBundle\Entity\User\User",
     *    statusCodes={
     *        201="Returned when created",
     *        400="Returned when validation fails",
     *    }
     * )
     *
     * @param Request $request
     *
     * @return View
     */
    public function createAction(Request $request)
    {
        $userManager = $this->get('kn.user_manager');

        $user = $userManager->createUser();

        $form = $this->createForm(UserType::class, $user, [
            'method' => Request::METHOD_POST
        ]);

        $form->handleRequest($request);

        if ($form->isValid()) {

            $userManager->save($user);

            return $this->created($user);
        }

        return $this->badRequest($form);
    }

    /**
     * Update user details
     *
     * @Put("/users/{id}", name="kn.user.update")
     *
     * @ParamConverter("user", class="Knipster\AppBundle\Entity\User\User")
     *
     * @ApiDoc(
     *    section="Users",
     *    description="Update user details",
     *    parameters={
     *       {"name" = "country", "dataType" = "object", "format" = "Country object {id : int, code: string, name: string}"}
     *    },
     *    input="Knipster\AppBundle\Form\Type\User\UserType",
     *    output="Knipster\AppBundle\Entity\User\User",
     *        statusCodes={
     *            200="Returned when updated",
     *            400="Returned when validation fails",
     *        }
     * )
     *
     * @param User    $user
     * @param Request $request
     *
     *
     * @return View
     */
    public function updateAction(User $user, Request $request)
    {
        $userManager = $this->get('kn.user_manager');

        $form = $this->createForm(UserType::class, $user, [
            'method' => Request::METHOD_PUT
        ]);

        $form->handleRequest($request);

        if ($form->isValid()) {

            $userManager->save($user);

            return $this->ok($user);
        }

        return $this->badRequest($form);
    }
}