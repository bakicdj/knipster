<?php


namespace Knipster\AppBundle\Controller\Money;


use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\View\View;
use Knipster\AppBundle\Controller\BaseController;
use Knipster\AppBundle\Entity\User\User;
use Knipster\AppBundle\Exception\TransactionException;
use Knipster\AppBundle\Form\Type\Money\TransactionType;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class TransactionController
 *
 * @package Doctrine\Bundle\DoctrineBundle\Controller\Money
 */
class TransactionController extends BaseController
{
    /**
     * Register new user
     *
     * @Post("/users/{id}/transactions", name="kn.transaction.create")
     *
     * @ParamConverter("user", class="Knipster\AppBundle\Entity\User\User")
     *
     * @ApiDoc(
     *    section="Users",
     *    description="Create new transaction",
     *    input="Knipster\AppBundle\Form\Type\Money\TransactionType",
     *    output="Knipster\AppBundle\Entity\Money\Transaction",
     *        statusCodes={
     *            201="Returned when created",
     *            400="Returned when validation fails",
     *        }
     * )
     *
     * @param User $user
     * @param Request $request
     *
     * @return View
     */
    public function createAction(User $user, Request $request)
    {
        $transactionManager = $this->get('kn.transaction_manager');

        $transaction = $transactionManager->createTransaction($user);

        $form = $this->createForm(TransactionType::class, $transaction, [
            'method' => Request::METHOD_POST
        ]);

        $form->handleRequest($request);

        if ($form->isValid()) {
            try {
                $transactionManager->save($transaction);
            } catch (TransactionException $e) {
                return $this->badRequest($e->getMessage());
            }

            return $this->created($transaction);
        }

        return $this->badRequest($form);
    }
}