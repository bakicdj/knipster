<?php


namespace Knipster\AppBundle\Controller;


use Doctrine\ORM\Query;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\View\View;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class BaseController
 *
 * @package Knipster\AppBundle\Controller
 */
class BaseController extends FOSRestController
{
    /**
     * Return 400 bad request with data
     *
     * @param $data
     *
     * @return View
     */
    protected function badRequest($data = null)
    {
        if(is_string($data)) {
            $data = [
                'error' => [
                    'type' => 'BadRequest',
                    'message' => $data,
                    'data' => null
                ]
            ];
        }

        if($data instanceof Form) {
            $data = [
                'error' => [
                    'type' => 'BadRequest',
                    'message' => 'Validation failed',
                    'data' => $this->get('kn.form_errors_serializer')
                        ->serializeFormErrors($data)
                ]
            ];
        }

        return $this->view($data, Codes::HTTP_BAD_REQUEST);
    }

    /**
     * @return View
     *
     * @throws NotFoundHttpException
     */
    protected function notFound()
    {
        throw new NotFoundHttpException('Not found');
    }

    /**
     * Return ok response
     *
     * @param mixed $data
     * @param mixed $meta
     * @param array $groups
     *
     * @return View
     */
    protected function ok($data = null, $meta = null, array $groups = [])
    {
        $view =  $this->view([
            'data' => $data,
            'meta' => $meta
        ], Codes::HTTP_OK);

        if (!empty($groups)) {
            $view->getSerializationContext()->enableMaxDepthChecks()->setGroups($groups);
        }

        return $view;
    }

    /**
     * Paginate collection
     *
     * @param array|Query $data
     * @param Request     $request
     * @param array       $meta
     *
     * @return View
     */
    protected function paginatedCollection($data, Request $request, $meta = [])
    {
        $paginator = $this->get('cg.query_paginator');

        $paginator->paginate($data, $request);

        return $this->ok(
            $paginator->getItems(),
            array_merge($paginator->getPaginationData(), $meta)
        );
    }

    /**
     * Return 201
     *
     * @param mixed $data
     * @param array $meta
     * @param array $groups
     *
     * @return View
     */
    protected function created($data, array $meta = [], array $groups = [])
    {
        $view = $this->view([
            'data' => $data,
            'meta' => $meta
        ], Codes::HTTP_CREATED);

        if (!empty($groups)) {
            $view->getSerializationContext()->enableMaxDepthChecks()->setGroups($groups);
        }

        return $view;
    }
}