<?php


namespace Knipster\AppBundle\Controller\Report;


use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\View\View;
use Knipster\AppBundle\Controller\BaseController;
use Knipster\AppBundle\DTO\Report\ReportFilter;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ReportController
 *
 * @package Knipster\AppBundle\Controller\Report
 */
class ReportController extends BaseController
{
    /**
     * Generate report
     *
     * @Get("/report", name="kn.report.generate")
     *
     * @ApiDoc(
     *    section="Report",
     *    description="Generate report",
     *    output="array<Knipster\AppBundle\DTO\Report\Report> as single report",
     *    filters={
     *        {"name"="from", "dataType"="Date in Y-m-d format"},
     *        {"name"="to", "dataType"="Date in Y-m-d format"}
     *    },
     *    statusCodes={
     *        201="Returned when ok"
     *    }
     * )
     *
     * @param Request $request
     *
     * @return View
     */
    public function generateAction(Request $request)
    {
        $report = $this->get('kn.transaction_manager')->generateReport(
            ReportFilter::createFromSymfonyRequest($request)
        );

        return $this->ok($report);
    }
}