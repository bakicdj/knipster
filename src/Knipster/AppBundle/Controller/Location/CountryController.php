<?php


namespace Knipster\AppBundle\Controller\Location;


use FOS\RestBundle\Controller\Annotations\Get;
use Knipster\AppBundle\Controller\BaseController;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * Class CountryController
 *
 * @package Knipster\AppBundle\Controller\Location
 */
class CountryController extends BaseController
{
    /**
     * List all countries
     *
     * @Get("/countries", name="kn.country.all")
     *
     * @ApiDoc(
     *    section="Countries",
     *    description="List all countries",
     *    output = "array<Knipster\AppBundle\Entity\Location\Country> as countries"
     * )
     *
     * @return View
     */
    public function allAction()
    {
        $countries = $this->get('kn.country_manager')->findAll();

        return $this->ok($countries);
    }
}