<?php


namespace Knipster\AppBundle\Service\Manager\Location;


use Knipster\AppBundle\Entity\Location\Country;
use Knipster\AppBundle\Service\Manager\AbstractManager;

/**
 * Class CountryManger
 *
 * @package Knipster\AppBundle\Service\Manager\Location
 */
class CountryManger extends AbstractManager
{
    /**
     * Get all countries
     *
     * @return Country[]
     */
    public function findAll()
    {
        return $this->em
            ->getRepository(Country::class)
            ->findAll();
    }
}