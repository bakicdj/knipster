<?php


namespace Knipster\AppBundle\Service\Manager\User;


use Knipster\AppBundle\Entity\Location\Country;
use Knipster\AppBundle\Entity\User\User;
use Knipster\AppBundle\Service\Manager\AbstractManager;

/**
 * Class UserManager
 *
 * @package Knipster\AppBundle\Service\Manager\User
 */
class UserManager extends AbstractManager
{
    /**
     * Create new User instance
     *
     * @param string  $firstName
     * @param string  $lastName
     * @param string  $email
     * @param string  $gender
     * @param Country $country
     *
     * @return User
     */
    public function createUser(
        $firstName = null,
        $lastName = null,
        $email = null,
        $gender = null,
        Country $country = null
    )
    {
        $user = new User();

        $user->setFirstName($firstName);
        $user->setLastName($lastName);
        $user->setEmail($email);
        $user->setGender($gender);
        $user->setCountry($country);

        return $user;
    }

    /**
     * @param User $user
     */
    public function save($user)
    {
        $user->setBonusPercent(
            $this->getBonusPercent()
        );

        parent::save($user);
    }


    /**
     * Get random bonus percent for user
     *
     * @return int
     */
    public function getBonusPercent()
    {
        return random_int(
            User::BONUS_PERCENT_MIN,
            User::BONUS_PERCENT_MAX
        );
    }
}