<?php


namespace Knipster\AppBundle\Service\Manager\Money;


use Doctrine\Common\Collections\ArrayCollection;
use Knipster\AppBundle\DTO\Report\Report;
use Knipster\AppBundle\DTO\Report\ReportFilter;
use Knipster\AppBundle\Entity\Money\Transaction;
use Knipster\AppBundle\Entity\User\User;
use Knipster\AppBundle\Event\Money\TransactionCreatedEvent;
use Knipster\AppBundle\Event\Money\TransactionCreatingEvent;
use Knipster\AppBundle\KnipsterEvents;
use Knipster\AppBundle\Service\Manager\AbstractManager;


/**
 * Class TransactionManager
 *
 * @package Knipster\AppBundle\Service\Manager\Money
 */
class TransactionManager extends AbstractManager
{
    /**
     * Create new Transaction instance
     *
     * @param User  $User
     * @param float $amount
     *
     * @return Transaction
     */
    public function createTransaction(User $User = null, $amount = null)
    {
        $transaction = new Transaction();

        $transaction->setAmount($amount);
        $transaction->setUser($User);

        return $transaction;
    }

    /**
     * Save transaction
     *
     * @param Transaction $transaction
     */
    public function save($transaction)
    {
        $this->eventDispatcher->dispatch(
            KnipsterEvents::TRANSACTION_CREATING,
            TransactionCreatingEvent::create($transaction)
        );

        parent::save($transaction);

        $this->eventDispatcher->dispatch(
            KnipsterEvents::TRANSACTION_CREATED,
            TransactionCreatedEvent::create($transaction)
        );
    }

    /**
     * Get balance for given user
     *
     * @param User $user
     *
     * @return float
     */
    public function getBalance(User $user)
    {
        return $this->em
            ->getRepository(Transaction::class)
            ->getBalance($user);
    }

    /**
     * Check if transaction qualifies for bonus
     *
     * @param Transaction $transaction
     *
     * @return bool
     */
    public function qualifiesForBonus(Transaction $transaction)
    {
        $userTransactionCount = $this->em
            ->getRepository(Transaction::class)
            ->getCountForUser($transaction->getUser());

        return ($userTransactionCount % 3) === 0;
    }

    /**
     * Calculate bonus amount for given transaction
     *
     * @param Transaction $transaction
     *
     * @return float
     */
    public function calculateBonusAmount(Transaction $transaction)
    {
        return floatval(
            ($transaction->getUser()->getBonusPercent() / 100) * $transaction->getAmount()
        );
    }

    /**
     * Generate admin report
     *
     * @param ReportFilter $reportFilter
     *
     * @return Report[]|ArrayCollection
     */
    public function generateReport(ReportFilter $reportFilter)
    {
        $result = $this->em
            ->getRepository(Transaction::class)
            ->generateReport(
                $reportFilter->getDateFrom(),
                $reportFilter->getDateTo()
            );

        $reportCollection = new ArrayCollection();

        foreach ($result as $report) {
            $reportCollection->add(
                Report::createFromMysqlResult($report)
            );
        }

        return $reportCollection;
    }
}