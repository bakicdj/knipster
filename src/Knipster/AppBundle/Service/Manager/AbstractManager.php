<?php


namespace Knipster\AppBundle\Service\Manager;


use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class Manager
 *
 * @package Knipster\AppBundle\Service\Manager
 */
abstract class AbstractManager
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var EventDispatcherInterface
     */
    protected $eventDispatcher;

    /**
     * Set basic managers dependencies
     *
     * @param EntityManager            $em
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function setDependencies(EntityManager $em, EventDispatcherInterface $eventDispatcher)
    {
        $this->em = $em;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * Update doctrine entity
     *
     * @param object $entity
     */
    public function update($entity)
    {
        $this->em->persist($entity);
        $this->em->flush();
    }

    /**
     * Save entity
     *
     * @param $entity
     */
    public function save($entity)
    {
        $this->update($entity);
    }

    /**
     * Refresh entity
     *
     * @param object $entity
     */
    public function refresh($entity)
    {
        $this->em->refresh($entity);
    }

    /**
     * Remove doctrine entity
     *
     * @param object $entity
     */
    public function remove($entity)
    {
        $this->em->remove($entity);
        $this->em->flush();
    }

    /**
     * Persist entity
     *
     * @param $entity
     */
    public function persist($entity)
    {
        $this->em->persist($entity);
    }

    /**
     * Flush entity
     *
     * @param $entity
     */
    public function flush($entity)
    {
        $this->em->flush($entity);
    }
}