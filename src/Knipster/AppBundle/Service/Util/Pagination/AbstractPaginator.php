<?php

namespace Knipster\AppBundle\Service\Util\Pagination;


use Symfony\Component\HttpFoundation\Request;


/**
 * Interface Paginator
 *
 * @package Knipster\AppBundle\Service\Util\Pagination
 */
abstract class AbstractPaginator
{
    const DEFAULT_LIMIT = 10;
    const DEFAULT_OFFSET = 0;

    /**
     * @var array
     */
    protected $items;

    /**
     * @var int
     */
    protected $limit;

    /**
     * @var int
     */
    protected $offset;

    /**
     * @var int
     */
    protected $totalCount;

    /**
     * @return array
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @return int
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @return int
     */
    public function getOffset()
    {
        return $this->offset;
    }

    /**
     * @return int
     */
    public function getTotalCount()
    {
        return $this->totalCount;
    }

    /**
     * @param Request $request
     */
    public function bindRequest(Request $request)
    {
        $this->limit  = $request->query->getInt('limit', self::DEFAULT_LIMIT);
        $this->offset = $request->query->getInt('offset', self::DEFAULT_OFFSET);
    }

    /**
     * Get data for serialization
     *
     * @return array
     */
    public function getPaginationData()
    {
        return [
            'pagination' => [
                'limit'      => $this->getLimit(),
                'offset'     => $this->getOffset(),
                'totalCount' => $this->getTotalCount()
            ]
        ];
    }

    abstract public function paginate($data, Request $request);
}