<?php


namespace Knipster\AppBundle\Service\Util\Pagination;


use Doctrine\ORM\Query;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class QueryPaginator
 *
 * @package Knipster\AppBundle\Service\Util\Pagination
 */
class QueryPaginator extends AbstractPaginator
{
    /**
     * Create pagination
     *
     * @param Query   $data
     * @param Request $request
     *
     * @return array
     */
    public function paginate($data, Request $request)
    {
        $this->bindRequest($request);

        $this->totalCount = count($data->getResult());

        $this->items = $data->setMaxResults($this->limit)
            ->setFirstResult($this->offset)
            ->getResult();
    }
}