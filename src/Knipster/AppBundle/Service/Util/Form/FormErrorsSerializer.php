<?php


namespace Knipster\AppBundle\Service\Util\Form;


use Symfony\Component\Form\Form;

/**
 * Class FormErrorsSerializer
 *
 * @package Knipster\AppBundle\Service\Util\Form
 */
class FormErrorsSerializer
{

    /**
     * Serialize errors for given form
     *
     * @param Form       $form
     * @param bool|false $flatArray
     * @param bool|false $addFormName
     * @param string     $glueKeys
     *
     * @return array
     */
    public function serializeFormErrors(Form $form, $flatArray = false, $addFormName = false, $glueKeys = '_')
    {
        $errors = array();
        $errors['global'] = array();
        $errors['fields'] = array();

        foreach ($form->getErrors() as $error) {
            $errors['global'][] = $error->getMessage();
        }

        $errors['fields'] = $this->serialize($form);

        if ($flatArray) {
            $errors['fields'] = $this->arrayFlatten($errors['fields'],
                $glueKeys, (($addFormName) ? $form->getName() : ''));
        }


        return $errors;
    }

    /**
     * Do serialization
     *
     * @param Form $form
     *
     * @return array
     */
    private function serialize(Form $form)
    {
        $localErrors = array();
        foreach ($form->getIterator() as $key => $child) {

            foreach ($child->getErrors() as $error){
                $localErrors[$key] = $error->getMessage();
            }

            if (count($child->getIterator()) > 0) {
                $localErrors[$key] = $this->serialize($child);
            }
        }

        return $localErrors;
    }

    /**
     * Flatten the array
     *
     * @param array  $array
     * @param string $separator
     * @param string $flattenedKey
     *
     * @return array
     */
    private function arrayFlatten(array $array, $separator = "_", $flattenedKey = '') {
        $flattenedArray = array();
        foreach ($array as $key => $value) {

            if(is_array($value)) {

                $flattenedArray = array_merge($flattenedArray,
                    $this->arrayFlatten($value, $separator,
                        (strlen($flattenedKey) > 0 ? $flattenedKey . $separator : "") . $key)
                );

            } else {
                $flattenedArray[(strlen($flattenedKey) > 0 ? $flattenedKey . $separator : "") . $key] = $value;
            }
        }
        return $flattenedArray;
    }
}