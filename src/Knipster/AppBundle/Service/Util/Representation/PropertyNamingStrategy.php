<?php


namespace Knipster\AppBundle\Service\Util\Representation;


use JMS\Serializer\Naming\PropertyNamingStrategyInterface;
use JMS\Serializer\Metadata\PropertyMetadata;

/**
 * Class PropertyNamingStrategy
 *
 * @package Knipster\AppBundle\Service\Util\Representation
 */
class PropertyNamingStrategy implements PropertyNamingStrategyInterface
{
    /**
     * @param PropertyMetadata $property
     *
     * @return string
     */
    public function translateName(PropertyMetadata $property)
    {
        return $property->serializedName ?: $property->name;
    }
}
