<?php


namespace Knipster\AppBundle;


/**
 * Class KnipsterEvents
 *
 * @package Knipster\AppBundle
 */
final class KnipsterEvents
{
    /**
     *  Before transaction is created
     */
    const TRANSACTION_CREATING = 'transaction.creating';

    /**
     *  After transaction is created
     */
    const TRANSACTION_CREATED = 'transaction.created';
}