<?php


namespace Knipster\AppBundle\Event\Kernel;


use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

/**
 * Class KernelExceptionSubscriber
 *
 * @package Knipster\AppBundle\Event\Kernel
 */
class KernelExceptionSubscriber
{
    /**
     * @var string
     */
    private $environment;

    /**
     * KernelExceptionSubscriber constructor.
     *
     * @param string $environment
     */
    public function __construct($environment)
    {
        $this->environment = $environment;
    }

    /**
     * On kernel error
     *
     * @param GetResponseForExceptionEvent $event
     */
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        // You get the exception object from the received event
        $exception = $event->getException();

        $exceptionClass = get_class($exception);

        $class = explode('\\', $exceptionClass);
        $type = array_pop($class);

        $message = array(
            'error' => array(
                'type' => $type,
                'message' => $exception->getMessage(),
                'data' => null
            )
        );

        if($this->environment === 'dev' || $this->environment === 'test') {
            $message['error'] = array_merge($message['error'], array(
                'file' => $exception->getFile(),
                'line' => $exception->getLine(),
                'trace' => $exception->getTraceAsString()
            ));
        }

        // Customize your response object to display the exception details
        $response = new Response();
        $response->setContent(json_encode($message));

        // HttpExceptionInterface is a special type of exception that
        // holds status code and header details
        if ($exception instanceof HttpExceptionInterface) {
            $response->setStatusCode($exception->getStatusCode());
            $response->headers->replace($exception->getHeaders());
        } else {
            $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        // Send the modified response object to the event
        $event->setResponse($response);
    }
}