<?php


namespace Knipster\AppBundle\Event\Money;


use Knipster\AppBundle\Entity\Money\Transaction;
use Symfony\Component\EventDispatcher\Event;


/**
 * Class TransactionCreatingEvent
 *
 * @package Knipster\AppBundle\Event\Money
 */
final class TransactionCreatingEvent extends Event
{
    /**
     * @var Transaction
     */
    private $transaction;

    /**
     * TransactionCreatingEvent constructor.
     *
     * @param Transaction $transaction
     */
    private function __construct(Transaction $transaction)
    {
        $this->transaction = $transaction;
    }

    /**
     * Create new TransactionCreatingEvent instance
     *
     * @param Transaction $transaction
     *
     * @return static
     */
    public static function create(Transaction $transaction)
    {
        return new static($transaction);
    }

    /**
     * @return Transaction
     */
    public function getTransaction()
    {
        return $this->transaction;
    }
}