<?php


namespace Knipster\AppBundle\Event\Money;


use Knipster\AppBundle\Exception\TransactionException;
use Knipster\AppBundle\KnipsterEvents;
use Knipster\AppBundle\Service\Manager\Money\TransactionManager;
use Knipster\AppBundle\Service\Manager\User\UserManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;


/**
 * Class TransactionEventSubscriber
 *
 * @package Knipster\AppBundle\Event\Money
 */
class TransactionEventSubscriber implements EventSubscriberInterface
{
    const PRIORITY_1 = 1;
    const PRIORITY_2 = 2;

    /**
     * @var TransactionManager
     */
    protected $transactionManager;

    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * TransactionEventSubscriber constructor.
     *
     * @param TransactionManager $transactionManager
     * @param UserManager        $userManager
     */
    public function __construct(TransactionManager $transactionManager, UserManager $userManager)
    {
        $this->transactionManager = $transactionManager;
        $this->userManager        = $userManager;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            KnipsterEvents::TRANSACTION_CREATING => [
                ['lockOtherTransactionsForUser',             self::PRIORITY_1],
                ['disallowWithdrawalIfThereIsNoEnoughFunds', self::PRIORITY_2]
            ],
            KnipsterEvents::TRANSACTION_CREATED => [
                ['addBonusToTransactionIfQualified',         self::PRIORITY_1],
                ['unlockOtherTransactionsForUser',           self::PRIORITY_2],
            ]
        ];
    }

    /**
     * Prevent user to make concurrent transactions
     *
     * @param TransactionCreatingEvent $event
     *
     * @throws TransactionException
     */
    public function lockOtherTransactionsForUser(TransactionCreatingEvent $event)
    {
        $user = $event->getTransaction()->getUser();

        if ($user->isTransactionsLocked()) {
            throw new TransactionException('Your transactions has been locked');
        }

        $user->lockTransactions();

        $this->userManager->update($user);
    }

    /**
     * Check if there is enough withdrawable funds
     *
     * @param TransactionCreatingEvent $event
     *
     * @throws TransactionException
     */
    public function disallowWithdrawalIfThereIsNoEnoughFunds(TransactionCreatingEvent $event)
    {
        $transaction = $event->getTransaction();

        if ($transaction->isWithdrawal()) {
            if (($this->transactionManager->getBalance($transaction->getUser()) + $transaction->getAmount()) < 0) {
                throw new TransactionException('Insufficient funds');
            }
        }
    }

    /**
     * Add bonus if is every third
     *
     * @param TransactionCreatedEvent $event
     */
    public function addBonusToTransactionIfQualified(TransactionCreatedEvent $event)
    {
        $transaction = $event->getTransaction();

        if ($transaction->isDeposit()) {

            if ($this->transactionManager->qualifiesForBonus($transaction)) {
                $transaction->setBonusAmount(
                    $this->transactionManager->calculateBonusAmount($transaction)
                );

                $this->transactionManager->update($transaction);
            }
        }
    }

    /**
     * Allow user to make transactions
     *
     * @param TransactionCreatedEvent $event
     */
    public function unlockOtherTransactionsForUser(TransactionCreatedEvent $event)
    {
        $user = $event->getTransaction()->getUser();

        $user->unlockTransactions();

        $this->userManager->update($user);
    }
}