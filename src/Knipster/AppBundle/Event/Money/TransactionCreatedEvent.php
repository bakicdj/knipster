<?php


namespace Knipster\AppBundle\Event\Money;


use Knipster\AppBundle\Entity\Money\Transaction;
use Symfony\Component\EventDispatcher\Event;


/**
 * Class TransactionCreatedEvent
 *
 * @package Knipster\AppBundle\Event\Money
 */
final class TransactionCreatedEvent extends Event
{
    /**
     * @var Transaction
     */
    private $transaction;

    /**
     * TransactionCreatedEvent constructor.
     *
     * @param Transaction $transaction
     */
    private function __construct(Transaction $transaction)
    {
        $this->transaction = $transaction;
    }

    /**
     * Create new TransactionCreatedEvent instance
     *
     * @param Transaction $transaction
     *
     * @return static
     */
    public static function create(Transaction $transaction)
    {
        return new static($transaction);
    }

    /**
     * @return Transaction
     */
    public function getTransaction()
    {
        return $this->transaction;
    }
}