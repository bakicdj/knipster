<?php


namespace Knipster\AppBundle\Exception;


/**
 * Class BaseException
 *
 * @package Knipster\AppBundle\Exception
 */
class BaseException extends \Exception
{

}