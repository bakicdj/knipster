<?php


namespace Knipster\AppBundle\Exception;


/**
 * Class InvalidArgumentException
 *
 * @package Knipster\AppBundle\Exception
 */
class InvalidArgumentException extends BaseException
{

}