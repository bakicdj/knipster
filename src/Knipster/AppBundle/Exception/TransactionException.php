<?php


namespace Knipster\AppBundle\Exception;


/**
 * Class TransactionException
 *
 * @package Knipster\AppBundle\Exception
 */
class TransactionException extends BaseException
{

}