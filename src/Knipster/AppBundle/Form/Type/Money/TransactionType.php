<?php


namespace Knipster\AppBundle\Form\Type\Money;


use Knipster\AppBundle\Entity\Money\Transaction;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\DataTransformer\NumberToLocalizedStringTransformer;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class TransactionType
 *
 * @package Knipster\AppBundle\Form\Type\Money
 */
class TransactionType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('amount', NumberType::class, [
            'required'      => true,
            'scale'         => 2,
            'rounding_mode' => NumberToLocalizedStringTransformer::ROUND_HALF_DOWN
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class'         => Transaction::class,
            'allow_extra_fields' => true,
            'csrf_protection'    => false
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return '';
    }
}
{

}