<?php


namespace Knipster\AppBundle\Form\Type\User;


use Knipster\AppBundle\Entity\Location\Country;
use Knipster\AppBundle\Entity\User\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class UserType
 *
 * @package Knipster\AppBundle\Form\Type\User
 */
class UserType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('firstName', TextType::class, [
            'required' => true,
            'constraints' => [
                new NotBlank()
            ]
        ]);

        $builder->add('lastName', TextType::class, [
            'required' => true,
            'constraints' => [
                new NotBlank()
            ]
        ]);

        $builder->add('email', EmailType::class, [
            'required' => true,
            'constraints' => [
                new Email()
            ]
        ]);

        $builder->add('gender', ChoiceType::class, [
            'required' => true,
            'choices' => [
                User::GENDER_FEMALE,
                User::GENDER_MALE
            ]
        ]);

        $builder->add('country', EntityType::class, [
            'required' => true,
            'class' => Country::class,
        ]);

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function(FormEvent $event) {

            $data = $event->getData();

            isset($data['country']['id']) ? $data['country'] = $data['country']['id'] : null;

            $event->setData($data);
        });
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class'         => User::class,
            'allow_extra_fields' => true,
            'csrf_protection'    => false
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return '';
    }
}