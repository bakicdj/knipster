server {
    server_name carneige.dev;
    root /var/www;

    location /api {
        try_files $uri /app_dev.php$is_args$args;
    }

    location /api_testing {
        try_files $uri /app_test.php$is_args$args;
    }

    location ~ ^/(app_dev|app_test)\.php(/|$) {
        root /var/www/web;
        fastcgi_pass unix:/run/php/php7.0-fpm.sock;
        fastcgi_split_path_info ^(.+\.php)(/.*)$;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
        fastcgi_param DOCUMENT_ROOT $realpath_root;
    }

    error_log /var/log/nginx/knipster.dev_error.log;
    access_log /var/log/nginx/knipster.dev_access.log;
}
