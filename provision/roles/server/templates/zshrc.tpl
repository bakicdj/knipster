export ZSH=$HOME/.oh-my-zsh

ZSH_THEME="jnrowe"

plugins=(git symfony2 composer)

export PATH=$HOME/bin:/usr/local/bin:$PATH

source $ZSH/oh-my-zsh.sh

cd /var/www

alias cc="php /var/www/bin/console c:cl"
alias dsu="php /var/www/bin/console d:s:u --force"
alias dfl="php /var/www/bin/console d:f:l"
alias dsv="php /var/www/bin/console d:s:v"
alias rdb="php /var/www/bin/console d:d:d --force && php /var/www/bin/console d:d:c && php /var/www/bin/console d:s:u --force && php /var/www/bin/console d:f:l"
